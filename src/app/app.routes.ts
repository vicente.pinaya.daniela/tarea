import { CVComponent } from "./components/cv/cv.component";
import { AgendaComponent } from "./components/agenda/agenda.component";
import { RouterModule, Routes } from "@angular/router";
import { InicioComponent } from "./components/inicio/inicio.component";


const APP_ROUTES: Routes = [
 {path:'inicio', component: InicioComponent},
 {path: 'cv', component: CVComponent},
 {path: 'agenda', component:AgendaComponent},
 {path: '**',pathMatch:'full', redirectTo: 'inicio'}
];
export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);