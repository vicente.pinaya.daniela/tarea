import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';


import { AppComponent } from './app.component';
import { CVComponent } from './components/cv/cv.component';
import { AgendaComponent } from './components/agenda/agenda.component';
// import { AppRoutingModule } from './app-routing.module';

//Rutas
import { APP_ROUTING } from './app.routes';
import { InicioComponent } from './components/inicio/inicio.component';



@NgModule({
  declarations: [
    AppComponent,
    CVComponent,
    AgendaComponent,
    InicioComponent,
  ],
  imports: [
    BrowserModule,
    // AppRoutingModule
    APP_ROUTING
   
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
